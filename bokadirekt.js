var utils = require('utils');
var links = [];
var links_list = [];
var services = [];
var tjanster_data = [];
var x = require('casper').selectXPath;
var clinicData = [];
var i = 0;
var z = 0;
var fs = require('fs');
var day = new Date();
var file = "data-"+day +".txt";

var casper = require('casper').create({
  verbose: true,
  logLevel: 'error',
  pageSettings: {
    userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
  },
});

function getLinks() {
    console.log('add links');
    
        var links = document.querySelectorAll('.list-group-item.bd-search-result.bd-unit-link');
        return Array.prototype.map.call(links, function(link) {
        return link.getAttribute('href');
        });
    

}
function getLinksloop() {
        console.log('getlinksloop');
        //var links = {};
        links = this.evaluate(getLinks);
        console.log(links);
    for (var i = 0; i < links.length; i++) {
        console.log('push links');
    links[i] = "http://www.bokadirekt.se" + links[i];
        links_list.push(links[i]);
    }

        if (!casper.visible('a#ctl00_MainContent__ucDataPager__btnNext.nasta[disabled="disabled"]'))
            {
            casper.thenClick('a#ctl00_MainContent__ucDataPager__btnNext.nasta');
             casper.then(getLinksloop); 
    } else {
        casper.echo("END")
    }
}

function getServices() {
    console.log('add services');
    var services = document.querySelectorAll('a.service-link');
    return Array.prototype.map.call(services, function(service) {
        return service.getAttribute('href');
   });
}

casper.start('http://www.bokadirekt.se/', function() {
    // search for 'casperjs' from google form
    console.log('filling form');
    this.echo(this.getTitle());
    this.sendKeys('#ctl00_MainContent__txtSearchWhat', 'frisör'); 
});

casper.thenClick(x('//*[@id="ctl00_MainContent__btnSearch"]'), function() {
    console.log('searching');
     casper.then(getLinksloop);
    //links = links.concat(this.evaluate(getLinks));
  });



function loopThroughLinks() {
  // Recurses until all links are processed
  if (i < 2) {
    this.echo('[LINK #' + i + '] ' + links_list[i]);
    getData.call(this, links_list[i]);
    i++;
    this.run(loopThroughLinks);
  } else {
        fs.write(file, JSON.stringify(clinicData), 'w');
        utils.dump(clinicData);
        this.exit();
  }
}

function getServices() {
    console.log('add services');
    var services = document.querySelectorAll('a.service-link');
    return Array.prototype.map.call(services, function(service) {
        return service.getAttribute('href');
   });
}

function getData(link, service) {
    this.start(link, function() {
    console.log('getData');
    
    this.page.injectJs('relative/local/path/to/jquery.js');

    // Get info about clinic
    var clinicName = this.evaluate(function() {
         return $('h2[itemprop="name"]').text();
    });       
    var streetAddress = this.evaluate(function() {
         return $('.table.table-light div[itemprop="streetAddress"]').text().replace(/[\n\t\r]/g,"").trim();
    });   
    var postalCode = this.evaluate(function() {
         return $('.table.table-light span[itemprop="postalCode"]').text().replace(/[\n\t\r]/g,"").trim();
    });     
    var city = this.evaluate(function() {
         return $('.table.table-light span[itemprop="addressLocality"]').text().replace(/[\n\t\r]/g,"").trim();
    }); 
    var description = this.evaluate(function() {
        return $('p[itemprop="description"]').text();    
    });
    var email = this.evaluate(function() {
        return $('span[itemprop="email"]').text();    
    });    
    var url = this.evaluate(function() {
        return $('span[itemprop="url"]').text();    
    }); 
    var openingHours= this.evaluate(function() {
         return $('th:contains("Öppettider") + td').text();
    });    
    var telephone= this.evaluate(function() {
         return $('th:contains("Telefon") + td').text();
    });
        
    //get practioner info    
    var practioner_selector = 'div.face-caption';
    var practitioners_info = this.getElementsInfo(practioner_selector); // an array of objectliterals
    var url_selector = this.evaluate(function() {
         return $('.col-sm-4.col-lg-3 im').getAttribute('url');
    });
        //*[@id="ctl00_MainContent__lvResources_ctrl0__imgResourceImage"]
        //*[@id="resource_11272"]
      
       // var youtubeimgsrc = document.getElementById("#resource_17737 img").src;
        //  console.log('url-selector ' + youtubeimgsrc);
    //var url_selector = '#resource_11272';
   // var url_info = this.getElementsInfo(url_selector);    
        


  var practitioners = [];
  for (var i = 0; i < practitioners_info.length; i++) {
    practitioners.push(practitioners_info[i].text);
    //practitioners.push(url_info[i].getAttribute("src"));

  }
                       
        utils.dump(practitioners);
        
        
            services = this.evaluate(getServices);
       //split = services.split("\n");
      
  

        this.echo(services.length + ' services found:');
        //utils.dump(services); 
    //enter practioners page and get info about services    
  if (services.length > 0) {
         this.each(services, function() { 
                z++; // change the link being opened (has to be here specifically)
            this.thenOpen(("http://www.bokadirekt.se" + services[z]), function() {
            this.page.injectJs('relative/local/path/to/jquery.js');
            casper.capture("bokadirektercice.png");
            this.echo(this.getTitle()); // display the title of page
                var name = this.getTitle();

        var name= this.evaluate(function() {
            return $('span[itemprop="name"]').text();            });  
        var description = this.evaluate(function() {
            return $('span[itemprop="description"]').text();    
        });
        
        var price = this.evaluate(function() {
            return $('span[itemprop="price"]').text();    
        });
        
        var time = this.evaluate(function() {
            return $('span[itemprop="value"]').text();    
        });

       var tjanster = {
           name: name,
           description: description,
           price: price,
           time: time,       
       };
            utils.dump(tjanster);
            tjanster_data.push(tjanster);
        });
                 
           
    }); 
            
    
    // Add the clinic data to the array
        
    var data = {
        clinicName: clinicName,
        streetAddress: streetAddress,
        postalCode: postalCode,
        city: city,
        description: description,
        telephone: telephone,
        email: email,
        openingHours: openingHours,
        url: url,
//        latitud: latitud,
//        longitud: longitud,
        tjanster: [tjanster_data], 
        behandlare: [practitioners],

    };
    clinicData.push(data);

    
		}   

  });
}
 

casper.wait(2000, function() {
                            
    casper.capture("bokadirekt.png");
    


        this.echo(links_list.length + ' links found:');
    utils.dump(links_list); 
   

    
});



 
casper.run(loopThroughLinks);




